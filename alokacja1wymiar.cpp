#include <iostream>
#include <cstdlib>
#include <fstream>
#define DLUGOSC_SLOWA_BIN 1024									//dana okreslajaca dlugosc bufora podczas operacji na plikach binarnych
using namespace std;


int main() {

int opcja;
int dlugosc;
string dane;
char *bufor=new char[DLUGOSC_SLOWA_BIN];

do{
cout<<"Program wczytujacy i zapisujacy dane do pliku"<<endl;
cout<<"1.Wczytywanie z pliku tekstowego"<<endl;
cout<<"2.Zapisywanie do pliku tekstowego"<<endl;
cout<<"3.Wczytywanie z pliku binarnego"<<endl;
cout<<"4.Zapisywanie do pliku binarnego"<<endl;
cout<<"0.Koniec"<<endl<<endl<<endl;
cin>>opcja;
 
switch(opcja){
	case 0: {
		break;
	}
	
	case 1:{
 	fstream plik;
 	string nazwa;
 	cout<<"Podaj nazwe pliku z ktorego chcesz wczytac dane: ";
 	cin>>nazwa;
 	plik.open(nazwa.c_str(), ios::in);							//otworzenie pliku, nadanie trybu wczytywania
 	if( plik.good()!=true ){									//sprawdzenie czy plik otworzyl sie poprawnie
 		cout<<"Nie mozna otworzyc pliku"<<endl;
 		break;
	}
 	getline(plik, dane);										//sciagniecie calej linii tekstu do zmiennej dana
 	dlugosc=dane.length();										//odczytanie dlugosci zmiennej
 	int *tablica=new int [dlugosc];								//zaalokowanie tablicy o potrzebnej dlugosci
 	for(int g=0;g<dlugosc;g++)									//przepisanie danych ze zmiennej dane do tablicy
 		tablica[g]=dane[g];	
 	cout<<"Wczytano"<<endl;
 	plik.close();												//zamkniecie pliku
	break;
	
 }
 case 2:{
 	fstream plik;
 	string nazwa;
 	cout<<"Podaj nazwe pliku do ktorego chcesz zapisac dane: ";
 	cin>>nazwa;
 	plik.open(nazwa.c_str(), ios::out);							//otworzenie pliku, nadanie trybu zapisu
 	if( plik.good()!=true ){									//sprawdzenie czy plik otworzyl sie poprawnie
 		cout<<"Nie mozna otworzyc pliku"<<endl;
 		break;
	}
 	plik<<dane;													//zapisanie danych strumieniowo
 	cout<<"Zapisano"<<endl;
 		plik.close(); 											//zamkniecie pliku
	break;
 }
 case 3:{
 	fstream plik;
 	string nazwa;
 	cout<<"Podaj nazwe pliku z ktorego chcesz wczytac dane: ";
 	cin>>nazwa;
 	plik.open(nazwa.c_str(), ios::in | ios::binary);			//otworzenie pliku, nadanie trybu wczytania oraz trybu binarnego
 	if( plik.good()!=true ){									//sprawdzenie czy plik otworzyl sie poprawnie
 		cout<<"Nie mozna otworzyc pliku"<<endl;
 		break;
	}
	
 	plik.read(bufor, DLUGOSC_SLOWA_BIN);						//wczytanie danych o okreslonej dlugosci
 	cout<<"Wczytano"<<endl;
	plik.close();												//zamkniecie pliku
	break;
 }
 case 4:{
 	fstream plik;
 	string nazwa;
  	cout<<"Podaj nazwe pliku do ktorego chcesz zapisac dane: ";
 	cin>>nazwa;
 	plik.open(nazwa.c_str(), ios::out | ios::binary);			//otworzenie pliku, nadanie trybu zapisu oraz trybu binarnego
 	if( plik.good()!=true ){									//sprawdzenie czy plik otworzyl sie poprawnie
 		cout<<"Nie mozna otworzyc pliku"<<endl;
 		break;
	}
 	
 	for(int g=0;g<DLUGOSC_SLOWA_BIN;g++)						//petla zapisujaca w pliku dane z bufora
 		plik.write(&bufor[0],DLUGOSC_SLOWA_BIN);
 	cout<<"Zapisano"<<endl;
 		plik.close();											//zamkniecie pliku
	break;
 }
}


}while(opcja!=0);


return 0;
}
