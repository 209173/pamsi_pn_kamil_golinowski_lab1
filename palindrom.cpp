#include <iostream>
#include <string>
using namespace std;

///Funkcja sprawdzajaca czy daney wyraz jest palindromem
bool jestPal(string wyraz, int i, int j){	
		if(i<j){											//warunek w ktorym sprawdza sie czy juz przekroczono polowe wyrazu
			if(wyraz[i]==wyraz[j]){							//jesli sa niepoprawne
			i++; j--;										//funkcja zwraca false i konczy sie wykonywac
				return jestPal(wyraz, i, j) ;				//jesli sa poprawne zwraca true							
			}
			else 
				return false;
		}
		else{
			return true;
		}	
}

//Glowna funkcja
int main(){
	
	string wyraz;
	cout<<"Wprowadz wyraz do sprawdzenia: ";
	cin>>wyraz;
	cout<<endl;
	int i=0;
	int dlugosc=wyraz.length()-1;
	
	if(jestPal(wyraz,i,dlugosc))
		cout<<"Wyraz jest palindromem!"<<endl;
	else
		cout<<"Wyraz nie jest palindromem"<<endl;
	
	return 0;
}
